package net.javaguides.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.javaguides.springboot.model.User;
import net.javaguides.springboot.repository.UserRepository;


@SpringBootApplication
public class Noobspringboot2Application implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(Noobspringboot2Application.class, args);
	}
	
	@Autowired
	private UserRepository userRepository;	
	
	
	@Override
	public void run(String... args) throws Exception {
		this.userRepository.save(new User("Carvin", "Talabucon", "ca@gmail.com"));
		this.userRepository.save(new User("Noob", "Anthony", "rv@gmail.com"));
		this.userRepository.save(new User("Basic", "Tony", "in@gmail.com"));
	}

}
